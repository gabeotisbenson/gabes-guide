module.exports = {
	moduleNameMapper: {
		'^~/(?<path>.*)': '<rootDir>/src/$1'
	}
};
