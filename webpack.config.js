const path = require('path');
const webpack = require('webpack');

const resolve = dir => path.join(__dirname, dir);

module.exports = {
	mode: 'production',
	devtool: 'source-map',
	resolve: {
		extensions: ['.ts', '.js'],
		alias: {
			'~': resolve('src')
		}
	},
	module: {
		rules: [
			{
				test: /\.ts$/u,
				exclude: /node_modules/u,
				use: {
					loader: 'babel-loader'
				}
			}
		]
	},
	plugins: [new webpack.optimize.AggressiveMergingPlugin()],
	output: {
		filename: '[name].js'
	}
};
