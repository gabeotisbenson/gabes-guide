import BrowserSync from 'browser-sync';
import del from 'del';
import dom from 'gulp-jsdom';
import eslint from 'gulp-eslint';
import Fiber from 'fibers';
import gulpPug from 'gulp-pug';
import gulpSass from 'gulp-sass';
import hashSrc from 'gulp-hash-src';
import hljs from 'highlight.js';
import htmlmin from 'gulp-htmlmin';
import markdown from 'gulp-markdown';
import named from 'vinyl-named';
import path from 'path';
import postcss from 'gulp-postcss';
import pug from 'pug';
import puglint from 'gulp-pug-linter';
import { Renderer } from 'marked';
import sass from 'sass';
import sourcemaps from 'gulp-sourcemaps';
import stylelint from 'gulp-stylelint';
import webpack from 'webpack-stream';
import webpackCompiler from 'webpack';
import webpackConfig from './webpack.config.js';
import {
	append,
	prepend
} from 'gulp-insert';
import {
	dest,
	watch as gulpWatch,
	parallel,
	series,
	src
} from 'gulp';

const CONTENT_STRING = '{{ CONTENT }}';
const SOURCE_DIRECTORY = path.resolve('./', 'src');
const OUTPUT_DIRECTORY = './dist';
const PUG_OPTIONS = { basedir: SOURCE_DIRECTORY };

const resolve = relativePath => path.join(SOURCE_DIRECTORY, relativePath);

// Configure gulp-sass
gulpSass.compiler = sass;

// Cleaners
const cleanTarget = targets => {
	const targetArr = Array.isArray(targets) ? targets : [targets];

	return targetArr.map(t => [OUTPUT_DIRECTORY, t].join('/'));
};
export const cleanHtml = () => del(cleanTarget('*.html'));
export const cleanCss = () => del(cleanTarget(['*.css', '*.map']));
export const cleanJs = () => del(cleanTarget(['*.js', '*.map']));
export const clean = parallel(cleanHtml, cleanCss, cleanJs);

// Markdown

export const compileMarkdown = () => {
	const escapeMap = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		'\'': '&#39;'
	};

	const escapeForHTML = input => input
		.replace(/(?<specialChar>[&<>'"])/gu, char => escapeMap[char]);

	const renderer = new Renderer();
	renderer.code = (code, language) => {
		const validLang = Boolean(language && hljs.getLanguage(language));

		const highlighted = validLang ? hljs.highlight(language, code).value : escapeForHTML(code);

		return `<pre><code class="hljs ${language}">${highlighted}</code></pre>`;
	};

	const template = pug.renderFile(resolve('pages/guide.pug'), PUG_OPTIONS);
	const templateTop = template.substring(0, template.indexOf(CONTENT_STRING));
	const templateBottom = template
		.replace(templateTop, '')
		.replace(CONTENT_STRING, '');

	return src(resolve('guides/*.md'))
		.pipe(markdown({ renderer }))
		.pipe(prepend(templateTop))
		.pipe(append(templateBottom))
		.pipe(dom(document => {
			const titleTag = document.querySelector('title');
			const descriptionTag = document.querySelector('meta[name="description"]');
			const topHeading = document.querySelector('h1');
			const topParagraph = document.querySelector('p');

			const { textContent: headingText } = topHeading;
			const { textContent: paragraphText } = topParagraph;

			titleTag.textContent = `Gabe's Guide for ${headingText}`;
			descriptionTag.setAttribute('content', paragraphText.replace(/\n/gu, ' '));
		}))
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(dest(OUTPUT_DIRECTORY));
};

// Markup
const compilePug = () => src(resolve('pages/index.pug'))
	.pipe(puglint({ failAfterError: true }))
	.pipe(gulpPug(PUG_OPTIONS))
	.pipe(dest(OUTPUT_DIRECTORY));

const injectHashes = () => src('./dist/*.html')
	.pipe(hashSrc({ build_dir: OUTPUT_DIRECTORY, src_path: 'js', exts: ['.js'] }))
	.pipe(hashSrc({ build_dir: OUTPUT_DIRECTORY, src_path: 'css', exts: ['.css'] }))
	.pipe(dest(OUTPUT_DIRECTORY));

export const html = series(
	parallel(compilePug, compileMarkdown),
	injectHashes
);

// Scripts
export const js = () => src(resolve('*.ts'))
	.pipe(eslint())
	.pipe(eslint.failAfterError())
	.pipe(named())
	.pipe(webpack(webpackConfig, webpackCompiler))
	.pipe(dest(OUTPUT_DIRECTORY));

// Stylesheets
export const css = () => src(resolve('scss/site.scss'))
	.pipe(stylelint({ failAfterError: true }))
	.pipe(sourcemaps.init())
	.pipe(gulpSass({ fiber: Fiber }).on('error', gulpSass.logError))
	.pipe(postcss())
	.pipe(sourcemaps.write('./'))
	.pipe(dest(OUTPUT_DIRECTORY));

// Watchers
const watchOptions = { ignoreInitial: false };

export const watchMarkdown = () => gulpWatch([resolve('**/*.md')], watchOptions, compileMarkdown);

export const watchHtml = () => gulpWatch([resolve('**/*.pug')], watchOptions, parallel(html, compileMarkdown));

export const watchCss = () => gulpWatch([resolve('**/*.scss')], watchOptions, css);

export const watchJs = () => gulpWatch([resolve('**/*.ts')], watchOptions, js);

export const watch = series(
	clean,
	parallel(watchMarkdown, watchHtml, watchCss, watchJs)
);

// Server
export const serve = () => {
	const changeEvent = 'change';
	const browserSync = BrowserSync.create();

	clean();

	[
		watchMarkdown,
		watchHtml,
		watchCss,
		watchJs
	].forEach(fn => fn().on(changeEvent, browserSync.reload));

	browserSync.init({ server: OUTPUT_DIRECTORY, port: 1234 });
};

// Default
export default series(
	clean,
	parallel(js, css),
	html
);
