module.exports = {
	extends: ['@gabegabegabe/eslint-config'],
	overrides: [
		{
			files: ['*.ts'],
			extends: '@gabegabegabe/eslint-config/typescript'
		}
	]
};
