export enum ThemePreference {
	LIGHT = 'light',
	DARK = 'dark'
}
