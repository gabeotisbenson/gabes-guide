import { DARK_CLASS } from '~/globals/css-classes';
import { ThemePreference } from '~/models/ThemePreference';

const STORAGE_PREFIX = 'gabes-guide';
const THEME_KEY = `${STORAGE_PREFIX}-theme`;
const {
	DARK: DEFAULT_THEME,
	LIGHT: ALT_THEME
} = ThemePreference;

const userPreferences = {
	get theme (): ThemePreference {
		const storedTheme = localStorage.getItem(THEME_KEY);

		if (storedTheme === ALT_THEME) return ALT_THEME;

		return DEFAULT_THEME;
	},
	set theme (themePreference: ThemePreference) {
		localStorage.setItem(THEME_KEY, themePreference);
		this.applyTheme();
	},
	applyTheme (): void {
		const body = document.querySelector('body');
		if (!body) return;

		if (this.theme === ThemePreference.DARK) body.classList.add(DARK_CLASS);
		else body.classList.remove(DARK_CLASS);
	},
	toggleTheme (): void {
		if (this.theme === ThemePreference.DARK) this.theme = ThemePreference.LIGHT;
		else this.theme = ThemePreference.DARK;
	}
};

export default userPreferences;
