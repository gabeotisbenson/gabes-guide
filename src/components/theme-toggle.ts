import { CLICK as CLICK_EVENT } from '~/globals/events';
import logger from '~/modules/logger';
import { THEME_TOGGLE as THEME_TOGGLE_SELECTOR } from '~/globals/css-selectors';
import userPreferences from '~/modules/user-preferences';

export const getThemeToggle = (): HTMLButtonElement => {
	const themeToggle: HTMLButtonElement|null = document.querySelector(THEME_TOGGLE_SELECTOR);

	if (!themeToggle) throw new Error('Theme toggle not found in DOM');

	return themeToggle;
};

export const enableThemeToggle = (): void => {
	try {
		const themeToggle = getThemeToggle();

		themeToggle.addEventListener(CLICK_EVENT, () => userPreferences.toggleTheme());
	} catch (err) {
		logger.error(err);
	}
};
