const HEADING_LEVELS = 6;
const HEADING_LINK_CONTENT = '🔗';
const HEADING_SELECTOR = new Array(HEADING_LEVELS)
	.fill(null)
	.map((h, i) => `h${i + 1}[id]`)
	.join(', ');

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
const constructHeadingLink = (heading: HTMLHeadingElement): HTMLAnchorElement => {
	const template = document.createElement('div');

	if (heading.textContent === null) throw new Error('Cannot construct header link for heading without text content');

	template.innerHTML = `
		<a class="heading-link" href="#${heading.id}" aria-label="${heading.textContent} Anchor">
			<span class="icon" aria-hidden="true">${HEADING_LINK_CONTENT}</span>
		</a>
	`.trim();

	const anchor = template.querySelector('a');

	if (!anchor) throw new Error('Failed to create anchor');

	return anchor;
};

export const applyHeadingLinks = (): void => {
	const headings = [...document.querySelectorAll(HEADING_SELECTOR)] as HTMLHeadingElement[];

	// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
	headings.forEach((heading: HTMLHeadingElement): void => {
		const link = constructHeadingLink(heading);
		heading.append(link);
	});
};
