import { applyHeadingLinks } from '~/components/heading-link';
import { enableThemeToggle } from '~/components/theme-toggle';
import ready from '@gabegabegabe/utils/dist/ready';

const main = (): void => {
	enableThemeToggle();

	if (window.location.pathname !== '/') applyHeadingLinks();
};

ready(main);
