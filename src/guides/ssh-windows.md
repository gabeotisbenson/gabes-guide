# Windows, SSH, and PuTTY
SSH is a protocol for remotely connecting to a host machine for work.  Since it
is used primarily in Linux and other Unix-like environments, it is necessary to
install some extra software on Windows systems to allow connecting to other
servers via SSH.  In this guide we'll walk through installing PuTTY, creating an
SSH key, and connecting to a remote server.

## Installing PuTTY
The PuTTY installer can be downloaded
[from its website](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).
You'll likely want the 64-bit MSI ('Windows Installer').  Simply download the
installer and run it, accepting all defaults unless you should wish to customize
the installation.  When the installer finishes, go ahead and launch PuTTY for
the first time.

## SSH Keys
When connecting to a server via SSH it's possible to just supply your username
and password (for the remote server).  However, allowing password authentication
is generally considered a security risk.  Instead, SSH keys are the preferred
means of authenticating.

An SSH key is actually a set of two keys: a public key and a private key.  The
private key is what is most important.  It is meant to be stored on your local
machine and your local machine alone.  Anyone who gains access to your private
key will have access to any and all servers you connect to using the private
key.  Protect it like your identity depends on it.  The public key is what will
be copied to the servers you'd like to authenticate with.  When connecting to a
server, the server looks at the public key it has on file for you and verifies
its signature with your private key.

Essentially, though, an SSH key is just that: a key.  However,
unlike a normal key, if your key is lost it's not necessary to change the lock
but rather to just mark that key as unaccepted.  This is why it's considered a
far superior authentication method than using a simple password.  See [this
article](https://wiki.archlinux.org/index.php/SSH_keys) for more information
about SSH keys.

### Generating an SSH Key

When you installed PuTTY a counterpart program was installed along with it:
PuTTYGen.  From the start menu, simply search for "puttygen" to find the
application and then launch it.  Once the application has loaded, we can create
a new key pair.

Before pressing the "Generate" button, it's advised to select a stronger
cryptography method than RSA.  RSA has been the standard for a long time, but
the newer Ed25519 method is considered to be superior.  So, in the "Parameters"
box at the bottom of the window, simply select the "Ed25519" bubble.  Now, click
the "Generate" button.  A loading bar will appear along with a message.  As the
message suggests, you should wiggle your cursor over the blank area in the
window in order to generate the entropy needed for the cryptography algorithm.

As you do this, the bar will progress until it is complete.  When it finishes
you'll have a few more fields to fill out:

**Key fingerprint** - This cannot be changed, it is specific to the key that was
just generated.

**Key comment** - A unique identifier for this key.  It can be whatever you'd
like, but it's typically recommended to name it something indicative of the
machine you'll be using the key on ("Gabe's Work Desktop" for example.)

**Key passphrase** - Optionally, you may specify a passphrase for the ssh key.
This is generally recommended as it provides an additional layer of security.

**Confirm passphrase** - If you enter a key passphrase, enter it again here.

Once you have configured these extra options to your satisfaction, click the
"Save public key" button and save it to a folder you'll be able to find later
on.  The filename should end in the ".pub" extension but otherwise can be named
whatever you like.  After that is saved, click the "Save private key" button and
do the same for this key.

That's it!  You now have your own SSH key pair that will allow you to securely
connect to a Linux server using SSH.

### Copying the Key to the Server
In order to authenticate with a server that forbids password-based login, you'll
need to supply the server with a copy of the public key.  To do so, you'll need
to provide an administrator a copy of your public key so that they can copy it
to the server for you.  Once you've done this you can move on to the next step.

### Configure PuTTY
Now that you've generated your SSH keys and an existing administrator of the
server has copied your public key to the server for you, you need to configure
PuTTY to use this new key.  To do so, first launch PuTTY.

When PuTTY opens, navigate on the left side to `Connection > SSH > Auth`.  In
the third pane under "Authentication parameters", click the "Browse..." button
next to the "Private key file for authentication:" input.  Navigate to the
_private_ key you saved from PuTTYGen and click ok.  The path to the private key
should now be populated in the input.

With that, you've configured PuTTY to use your ssh key.  From here, you can now
connect as you would normally with PuTTY but now you won't be prompted for a
password (unless you set a passphrase for your SSH key, in which case you simply
need to enter that passphrase when prompted.)
