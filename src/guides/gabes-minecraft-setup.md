# Gabe's Minecraft Setup
This guide is intended to walk you through my personal Minecraft setup, which
functions and looks better (in my humble opinion) than a vanilla Minecraft
install.

## MultiMC
First, you should install [MultiMC](https://multimc.org/#Download).

Once you have MultiMC installed, launch it and go through the initial setup of
signing into your Mojang account.

Then, click the "Add Instance" button.  Name it whatever you'd like, and select
the 1.16.4 release.  Finally, hit "OK".

## Fabric
Per its own description, Fabric is "a lightweight, experimental modding
toolchain for Minecraft."  Essentially, it's a mod for Minecraft that allows you
to install and run other mods/plugins on top of it.

### Installation
From within your MultiMC window, select your newly created instance.  On the
right side of the window, select "Edit Instance".  On the right side of this new
window, just click the "Install Fabric" button.  It will open another window
prompting you to select a version to install; simply select the latest version
and select "OK".

Congratulations!  You now have Fabric installed.

### Mods
The following are mods I run.  Note that I primarily play online, so none of
these modify the underlying Minecraft server but rather are client-side
modifications to improve my playing experience.

Each of these mods, can be installed using the following simple steps:

1. Download the mod using the link provided.
1. In MultiMC, after selecting your instance and selecting "Edit Instance",
	 navigate to the "Loader Mods" tab using the nav on the left.
1. Click "Add" on the right side of the window
1. Navigate to the downloaded `.jar` file and click "Open"
1. That's it!  The mod is now installed!

#### AppleSkin
AppleSkin is a simple mod that improves the HUD by providing more information
about your current hunger/exhaustion levels.

**[Download](https://www.curseforge.com/minecraft/mc-mods/appleskin)**

_Note: Make sure to download the Fabric version of this mod_

#### Fabric API
This is the core API for Fabric and is necessary to be able to run some of these
other mods.

**[Download](https://www.curseforge.com/minecraft/mc-mods/fabric-api)**

#### Hwyla
Hwyla (Here's What You're Looking At) simple mod that provides information on
blocks/mobs that your crosshair is currently targeting.

**[Download](https://www.curseforge.com/minecraft/mc-mods/hwyla)**

#### VoxelMap
VoxelMap gives you a nice little map overview of where you currently are in your
Minecraft world.

**[Download](https://www.curseforge.com/minecraft/mc-mods/voxelmap)**

#### OptiFine
OptiFine is a shader mod for Minecraft that drastically improves the look of the
game.  Your mileage may vary depending on the hardware in your PC, but if you
can run it I highly recommend it.

Installing OptiFine is only slightly more complicated than installing the other
mods, in that you'll actually need to download and install two mods.  Both are
installed using the same steps mentioned above.

**[Download OptiFabric](https://www.curseforge.com/minecraft/mc-mods/optifabric)**

**[Download OptiFine](https://www.optifine.net/downloads)**

### Resource Pack
My resource pack of choice is currently the BetterVanillaBuilding pack.  You can
download it at [CurseForge](https://www.curseforge.com/minecraft/texture-packs/bettervanillabuilding).

Install the resource pack by going to the "Resource Packs" tab of your "Edit
Instance" window, clicking "Add" on the right, navigating to the file, and
installing like you would a mod.  Simple as that!

### Shader Pack
After installing OptiFine, I recommend installing and using Sildur's Shaders.
You can download them at [his website](https://sildurs-shaders.github.io/downloads/).
I recommend downloading each of the versions of his shaderpacks and trying out
each to see which gives you the best balance between performance and appearance.

To install them, select your instance in the main MultiMC window, then click
"Minecraft Folder" on the right to open the folder for your instance in a file
manager window.  Inside there should be a "shaderpacks" folder; if it does not
exist, create it.  Then, simply move the downloaded shader pack into this
folder.

## Running the Game
At this point, you should have installed everything you need to have a setup
identical to mine.  So now, let's run the game!

From MultiMC, simply select your instance and click "Launch" on the right side
of the window.  This will launch Minecraft.

Once it loads, you'll need to enable the resource pack and shader pack.

To install the resource pack, click "Options", then "Resource Packs...".  Then,
hover over the resource pack on the left and click the "Play" icon to move it
into the "Selected" column.  Hit "Done" and you're done!

To install the shader pack, click "Options", then "Video Settings...", then
"Shaders...".  Click the shader you'd like to use, then click "Done".

You're done!  Play the game now and bask in how lovely it is!
