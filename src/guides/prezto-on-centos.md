# Prezto on CentOS 7
Prezto is a configuration framework for zsh; it enriches the command line
interface environment with sane defaults, aliases, functions, auto-completion,
and prompt themes.

Zsh is a shell for Linux that offers many features not available (or simply more
complicated to use/achieve) in the default shell, bash.

In short, Prezto+Zsh will make your life at the command line much easier and
hopefully more fun to boot!

## Installing git
To be able to use Prezto you need to have git installed on your system.  If you
don't already, just use this command to get it (git it?):

```bash
sudo yum install git
```

## Installing Zsh
Before we can use Prezto, we need to get zsh installed.  If you already have zsh
installed you can skip this step.  If not, read on.

Installing zsh is as simple as this:

```bash
sudo yum install zsh
```

Once that's done, you've got zsh!

## Installing Prezto
Now that you have zsh we can get Prezto going as well.  So for starters, launch
zsh:

```bash
zsh
```

You'll be greeted by a prompt asking how you'd like to configure zsh since this
is your first time running it.  We're actually going to skip the creation of a
config right now, as Prezto will take care of that for us later.  So, simply
enter `q` to move on.

Now, let's clone the Prezto repository:

```zsh
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
```

Next, we'll create a new zsh configuration by copying those provided by Prezto:

```zsh
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
```

**Note**: If you already have any of the given configuration files, `ln` will
cause an error.  In simple cases you can load prezto by adding the line `source
"${ZDOTDIR:-$HOME}/.zprezto/init.zsh"` to the bottom of your `.zshrc` and keep
the rest of your zsh configuration intact.  For more complicated setups, it is
recommended that you back up your original configs and replace them with the
provided prezto runcoms.

Lastly, set zsh as your default shell:

```zsh
chsh -s /bin/zsh
```

Now, either open a new terminal (if in a GUI environment) or log out and log
back in (if connected via ssh or console).

If everything worked correctly you should be greeted by a nice shiny new shell!
Welcome to the big leagues!
