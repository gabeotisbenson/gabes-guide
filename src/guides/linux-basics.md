# Linux Basics
This is intended to be a simple introduction to basic concepts and commands in
Linux that could be useful for someone just starting out.

## Core Concepts
### Everything is a File
This concept actually dates back to the early days of Unix and it refers to the
approach taken in Unix/Unix-like (such as Linux, MacOS) operating systems to
treat a wide range of input/output resources such as documents, directories,
hard-drives, modems, keyboards, printers, and even some inter-process and
network communications as simple streams of bytes exposed through the
filesystem name space.

The tangible result of this is that nearly every aspect of a Linux OS can be
controlled by editing the right file.  As such, there is no concept of a
"registry" in Linux like there is in Windows.  Additionally, even
directories/folders are simply other files with an additional "is-directory"
bit in place.

It's a somewhat vague concept, but it's important to know and will provide
context when exploring other aspects of Linux as an operating system and
learning how to work within the environment.

### Permissions
On a Linux system permissions are fairly simple and straightforward.  Every
file/directory has two owners: a user and a group.  A user can be a member of
any number of groups, and groups can contain any number of users.

A file's permissions on linux consist of three digits which represent the
owning user's rights, the owning group's rights, and other users' rights,
respectively.  Each of these digits can represent the ability to read, write,
or execute the file, or any combination of the three.  These individual
permissions are represented by integers: 4 = read, 2 = write, 1 = execute.  As
such, the sum of these integers is what is used to represent which combination
of the permissions is granted to a user, owner, or other.  E.g. '6' would
indicate the permission to read and write a file. '5' would represent the
permission to read and execute but not write a file.

It's important to note that there are additional "advanced" permissions that
are less common than the core read/write/execute functionality.  To learn more
about these permissions and permissions in general, check out the Arch Linux
wiki's [page](https://wiki.archlinux.org/index.php/File_permissions) about the
topic.

#### More Examples
Keeping in mind that a file's permissions are three digits representing the
different levels of ownership, the format would be \[User]\[Group]\[Others].
Keeping this in mind, a file with permissions set to **664** would indicate
that the _user_ ([6xx]) who owns the file has the ability to _read_ and _write_
(4 + 2 = 6) the file.  Similarly, the _group_ ([x6x]) which owns the file also
has the ability to _read_ and _write_ to the file.  All other users ([xx4])
would only have the ability to read (4) the file.

An executable file (such as a bash script or a binary similar to an .exe on
Windows) can only be run by a user if that user has permission to execute the
file.  A file with permissions of **764** would indicate that the owner of the
file can read, write, and execute the file, while group members could only read
and write the file, with other users only being able to read the file.  It's
important to note that directories can only be opened by users with execute
permissions on the directory.  As such, you'll often see the default
permissions of a directory set to **775**, indicating that the user and group
of the file can read/write/execute, and other users can read/execute.  A
directory with permissions of **664** would not be openable by any users, as
none of the three categories have execute permissions.

### Kernel, Operating System, and Distributions
Technically, the term "Linux" refers explicitly to the _kernel_ used by
operating systems casually referred to as "linux".  However, the kernel by
itself does not provide a functional operating system.  It is only the piece
of software that would let an operating system interact with underlying
hardware.  As such, you'll often see people correct others by stating that what
is referred to casually as "linux" is more accurately referred to as
"GNU/Linux" or "GNU+Linux".  GNU is a suite of low-level libraries and programs
that, when bundled with the Linux kernel, provide a complete operating system.

Further, things like "Ubuntu", "CentOS", "RedHat", "Arch", and "Debian" are all
names of _distributions_ of GNU+Linux, bundled with additional software to
give the end-user a certain type of experience.  There isn't really a similar
concept for Windows or MacOS, as there is only one official distribution of
each of those stacks of software.  An easy way to think about it is hamburgers.
Any type of burger probably comes with a beef patty and a bun.  The
combinations of other toppings sold by different restaurants can be thought of
as different "distributions" of a burger.

### Shells, Consoles, and Terminals
It's important to distinguish between these three terms, though they're often
used interchangeably.

In Linux, a shell is a piece of software that allows you
to execute other commands.  The default in most Linux distributions is
[bash](https://www.gnu.org/software/bash/).  Alternatives exist such as
[zsh](https://www.zsh.org/) and [fish](https://fishshell.com/).

A console typically refers to the presentation of a shell on a Linux machine
directly.  When booting up a Linux OS that does not have a graphical
environment, the basic black and white login screen you are greeted with is a
console.

A terminal, or more precisely a terminal _emulator_, is another piece of
software that can be run inside of a graphical environment that gives you access
to the shell of your choice.

For a more in-depth explanation of the differences between these three terms,
check out [this answer](https://askubuntu.com/questions/506510/what-is-the-difference-between-terminal-console-shell-and-command-line)
on Ask Ubuntu.

### Piping and Redirection
A very useful feature of most Linux shells is the ability to chain commands
(known as piping) and to redirect the output of commands.  For instance, say you
wanted to generate a text file containing the history of every command you've
entered on a Linux machine.  You could do the following:

```bash
history > commands.txt
```

This uses the `history` command to get the list of everything you've entered,
then using the `>` operator it redirects the output of that command from the
terminal to a file called `commands.txt`.  Now you have a file that you can
further manipulate, rather than just output dumped to your terminal or console.

Now that we have a file containing every command we've entered, let's see just
how many commands that is.  Using the `<` operator and the `wc` (short for "word
count") command we can see how many lines are in the file we've just generated:

```bash
wc < commands.txt
```

The `wc` command by default reads from "standard input", that is it takes input
from the user via the shell.  However, by using the `<` operator we have
redirected that input to instead come from the file `commands.txt`.  The result
will be the line, word, and byte count of the file.

Now let's say you just want to see how many times you've used the `yum` command.
Using the `|` operator we can "pipe" the output of one command into another,
effectively chaining commands together on the fly:

```bash
grep 'yum' commands.txt | wc
```

Using the `grep` command we search the file `commands.txt` for the string "yum".
The output is then piped into `wc` using the `|` operator.  `wc` in turn will
give us the line, word, and byte count for the resulting trimmed list.  Pretty
neat!

Let's say we wanted to do all of this at once, and instead of saving the list of
commands to a file, we want to save to a file the line, word, and byte count of every time
we've used the `yum` command.  We could do this:

```bash
history | grep 'yum' | wc > count.txt
```

The result would be a new file "count.txt" that contains just those counts and
nothing else.  Let's say you want to use this file to keep track of these counts
over time, so that you can use the data later on to see how your usage of the
`yum` command has changed over time.  Next time you generate these values you
could use the `>>` operator to _append_ to the file rather than creating or
overwriting it:

```bash
history | grep 'yum' | wc >> count.txt
```

This would print the numbers on a new line at the end of the file.  It's
important to remember that a single `>` will create a new file if it does not
exist, or, if it does exist, will replace its contents with the new content
being passed into the `>` operator.  Some Linux distributions will try and
protect you from accidentally overwriting a file's contents by requiring an
exclamation mark after the `>` operator to indicate that you're sure you want to
do this:

```bash
echo "New content" >! count.txt
```

Piping and redirection are invaluable tools that let you piece together all
kinds of things to make your life as a Linux user easy, or at least easier.

## Useful Commands

---

### man
`man`, short for "manual" is perhaps the most useful command on linux because
it allows you to see the manual (or "man-page") for almost every other command
you'll encounter.

#### Usage
`man [command]`

##### Example
```bash
man man
```

---

### cd
Short for "change directory", `cd` is the command used to move around the
filesystem.

#### Usage
`cd [path]`

##### Example
```bash
cd /home/gabe
```

___

### ls
`ls`, short for "list directory contents" also does just what the name implies:
print a list of all files and subdirectories contained within a directory.

#### Usage
`ls [path]`

#### Example
```bash
ls /home/gabe
```

---

### touch, mkdir, cp, mv, rm
These commands are what you use for basic file/directory manipulation.

`touch` creates a new, empty file.  Or, if the file already exists, it simply
updates the "last modified" value of the file.

```bash
touch newfile
```

`mkdir` creates a new, empty directory.  The `-p` flag can be used to say
"create this directory and all needed parent directories".

```bash
mkdir -p /home/gabe/newparent/newchild/newgrandchild
```

`cp` copies a file or directory.  Use the `-r` flag to recursively copy a
directory:

```bash
cp sourcefile.txt /home/gabe/targetfile.txt
```

```bash
cp -r allfiles/ /home/gabe/newplace
```

`mv` moves a file or directory:

```bash
mv sourcefile.txt /home/gabe/targetfile.txt
```

`rm` removes a file or directory.  Similar to `cp`, the `-r` flag is used to
recursively remove a directory:

```bash
rm file.txt
```

```bash
rm -r directory/
```


cp
mv
ln
rm
touch
mkdir
cat
chmod
chown
sudo
who
groups
systemctl
exit
