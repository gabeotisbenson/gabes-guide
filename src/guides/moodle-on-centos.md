# How to Install Moodle on CentOS 7
_A lot of credit for this guide is due to Digital Ocean for their own guides on
the topic._

This guide assumes that a fresh install of CentOS 7 has been set up, and that
the steps in the [initial CentOS 7 server setup](/articles/initial-centos-setup)
guide.

Moodle requires a web server, a database, and PHP.  We will use Nginx and
MariaDB for the web server and database, respectively.  This stack is commonly
referred to as "LEMP".

## Configure the Network
Since the vm was brought up as a clone of another system, it will have the
wrong IP address assigned to it.  We'll need to change this using the `nmtui`
application.  Simply launch it from a terminal:

```bash
nmtui
```

Select the network card, then navigate to "Edit" on the right.  Change the IP
address (and any other incorrect values) to what it should be, then save and
exit.  Reboot the vm:

```bash
sudo systemctl reboot
```

Once it has booted back up, you should be good to go!

## Update the System
Before installing new software on a system, it's always a good idea to update
first, to ensure you have the most recent versions of software available to
your OS.  On CentOS we do this using the `yum` utility:

```bash
sudo yum update
```

After the updates have been applied, it's usually a good idea to reboot, though
this is not always necessary.

Again, to reboot simply enter:

```bash
sudo systemctl reboot
```

## Install Nginx
Now that our system is connected to the internet and is up to date, we can
proceed.  In order to display web pages to our site visitors, we are going to
employ Nginx, a modern, efficient web server.

To add the CentOS 7 EPEL repository, open a terminal and use the following
command:

```bash
sudo yum install epel-release
```

Since we are using a `sudo` command, these operations get executed with root
privileges.  It will ask you for your regular user's password to verify that
you have permission to run commands with root privileges.

Now that the Nginx repository is installed on the server, install  Nginx using
the following `yum` command:

```bash
sudo yum install nginx
```

Afterward, your web server is installed.

Once it is installed, you can both start Nginx and enable it to run at boot
with the following command:

```bash
sudo systemctl enable --now nginx
```

You can do a spot check right away to verify that everything went as planned
by visiting the server's public IP address in your web browser.  If everything
is working correctly you will see a CentOS and Nginx information page.

## Install MariaDB
Now that we have our web server up and running, it is time to install MariaDB,
a MySQL drop-in replacement.  MariaDB is a community-developed fork of the
MySQL relational database management system.  Basically, it will organize and
provide access to databases where our site can store information.

Again, we can use `yum` to acquire and install our software.  This time, we'll
also install some other "helper" packages that will assist us in getting our
components to communicate with each other:

```bash
sudo yum install mariadb-server mariadb
```

When the installation is complete, we need to enable and start MariaDB with the
following command:

```bash
sudo systemctl enable --now mariadb
```

Now that our MySQL database is running, we want to run a simple security script
that will remove some dangerous defaults and lock down access to our database
system a little bit.  Start the interactive script by running:

```bash
sudo mysql_secure_installation
```

The prompt will ask you for your current root password.  Since you just
installed MariaDB, you most likely won't have one, so leave it blank by
pressing enter.  Then the prompt will ask you if you want to set a root
password.  Go ahead and enter `Y`, and follow the instructions:

```
mysql_secure_installation prompts:
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

New password: password
Re-enter new password: password
Password updated successfully!
Reloading privilege tables..
 ... Success!
```

For the rest of the questions, you should simply hit the "enter" key through
each prompt to accept the default values.  This will remove some sample users
and databases, disable remote root logins, and load these new rules so that
MariaDB immediately respects the changes we have made.

At this point, your database is set up and we can move on.

## Install PHP
PHP is the component of our setup that will process code to display dynamic
content.  It can run scripts, connect to our MySQL databases to get
information, and hand the processed content over to our web server to display.

The version of PHP that is available by default in CentOS 7 is 5.4.  This is
unfortunately quite old and a newer version of PHP is required my Moodle.  As
such, we'll need to enable an additional software repository that will let us
install the newer version of PHP and other needed modules.  Several
repositories exist that would allow us to install PHP 7, but we're going to
use the [IUS repository](https://ius.io/).

IUS offers an installation script for subscribing to their repository and
importing associated GPG keys.  Make sure you're in your home directory, and
retrieve the script using `curl`:

```bash
cd ~
```
```bash
curl 'https://setup.ius.io/' -o setup-ius.sh
```

Run the script:

```bash
sudo bash setup-ius.sh
```

Now that we have enabled the repository, we simply need to install PHP and its
related packages:

```bash
sudo yum install php73-fpm-nginx php73-cli php73-mysqlnd
```

There are some additional PHP modules required by Moodle that we'll also need
to install:

```bash
sudo yum install php73-json php73-xml php73-mbstring php73-xmlrpc php73-soap php73-gd php73-intl php73-opcache
```

### Configure the PHP Processor
We now have our PHP components installed, but we need to make some
configuration changes to make our setup more secure and to enable Nginx to use
PHP.

Open the main php-fpm configuration file with root privileges:

```bash
sudo -e /etc/php.ini
```

What we are looking for in this file is the parameter that sets
cgi.fix_pathinfo.  This will be commented out with a semi-colon and set to "1"
by default.

This is an extremely insecure setting because it tells PHP to attempt to
execute the closest file it can find if a PHP file does not match exactly.
This basically would allow users to craft PHP requests in a way that would
allow them to execute scripts that they shouldn't be allowed to execute.

We will change both of these conditions by uncommenting the line and setting it
to "0" like this:

```ini
cgi.fix_pathinfo=0
```

Save and close the file when you are finished.

Next, open the php-fpm configuration file `www.conf`:

```bash
sudo -e /etc/php-fpm.d/www.conf
```

Look for the block containing `listen = 127.0.0.1:9000`, which tells PHP-FPM to
listen on the loopback address at por 9000.  Comment this line with a
semicolon, and uncomment `listen = /run/php-fpm/www.sock` a few lines below.

```ini
; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
;listen = 127.0.0.1:9000
; WARNING: If you switch to a unix socket, you have to grant your webserver user
;          access to that socket by setting listen.acl_users to the webserver user.
listen = /run/php-fpm/www.sock
```

Next, look for the block containing `listen.acl_users` values, and uncomment
`listen.acl_users = nginx`:

```ini
; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
;listen.acl_users = apache,nginx
;listen.acl_users = apache
listen.acl_users = nginx
;listen.acl_groups =
```

Next, look for the lines that set the `listen.owner` and `listen.group` and
uncomment them.  They should look like this:

```ini
listen.owner = nobody
listen.group = nobody
```

Next, find the lines that set the `user` and `group` and change their values
from "apache" to "nginx":

```ini
user = nginx
group = nginx
```

Exit and save the file.

Lastly, make sure that Nginx is using the correct socket path to handle PHP
files.  Start by opening `/etc/nginx/conf.d/php-fpm.conf`:

```bash
sudo -e /etc/nginx/conf.d/php-fpm.conf
```

`php-fpm.conf` defines an **upstream**, which can be referenced by other Nginx
configuration directives.  Inside the upstream block, use a `#` to comment out
`server 127.0.0.1:9000;`, and uncomment `server unix:/run/php-fpm/www.sock;`:

```nginxconf
# PHP-FPM FastCGI server
# network or unix domain socket configuration

upstream php-fpm {
	#server 127.0.0.1:9000;
	server unix:/run/php-fpm/www.sock;
}
```

Exit and save the file.

Now, we just need to enable and start our PHP processor by typing:

```bash
sudo systemctl enable --now php-fpm
```

## Create Nginx Server Block
Now that we have all of the required components installed and configured, we
need to create our server block for Nginx to know where the site we're
hosting is located.

On CentOS, these server blocks are located at `/etc/nginx/conf.d/`.  So, let's
create our server block in that directory, and name it whatever the domain
you're wanting to host is, e.g. `learn.caes.uga.edu.conf`:

```bash
sudo -e /etc/nginx/conf.d/learn.caes.uga.edu.conf
```

In this file, we'll construct our server block.  You can simply copy and paste
the example below, albeit a few changes will be necessary.

```nginxconf
server {
	listen 80;
	server_name learn.caes.uga.edu;

	root /var/www/learn.caes.uga.edu/html/;
	index index.php index.html index.htm;

	location ~ ^(.+\.php)(.*)$ {
		root /var/www/learn.caes.uga.edu/html/;
		fastcgi_split_path_info ^(.+\.php)(.*)$;
		fastcgi_index	index.php;
		fastcgi_pass   php-fpm;
		include /etc/nginx/mime.types;
		include fastcgi_params;
		fastcgi_param PATH_INFO $fastcgi_path_info;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	}

	location / {
		try_files $uri $uri/ =404;
	}

	location /dataroot/ {
		internal;
		alias /var/www/learn.caes.uga.edu/data/;
	}
}
```

The following fields are what will likely need to be changed for your specific
instance:

- `server_name` should be set to the domain for your site.  If you would like
	the site to be addressible under several domains, enter them all on the same
	line, separated by a space.
-	`root` should be set to the web root of your site.  On CentOS, it's best to
	create a folder under `/var/www/` named whatever the primary domain of your
	site will be, with a second folder inside called `html`.  **Note:** Be sure to
	change this both in the `server` scope and within the PHP `location` scope.
-	`alias` inside the `location /dataroot/` block should be set to the match the
	domain for your site, with `/data/` appended to the end.

After you have made your changes, save and close the file.

Now, simply reload Nginx:

```bash
sudo systemctl restart nginx
```

## Configure the Firewall
At this stage, your vm is running a fully-functional web server that will
respond to requests for the domain you configured in the previous step.
However, if you visit the URL in a browser that is not on the vm itself you'll
notice that it fails to load.  This is because the firewall currently running
on the vm is currently block web traffic.  To allow web traffic to reach our
server, we'll need to add some exceptions to the firewall.  In addition to
allowing SSH traffic like we during the initial server setup, we also need to
allow HTTP and HTTPS connections over ports 80 and 443, respectively.  Luckily,
this is very straightforward and can be achieved with three simple commands:

```bash
sudo firewall-cmd --permanent --add-service=http
```
```bash
sudo firewall-cmd --permanent --add-service=https
```
```bash
sudo firewall-cmd --reload
```

After entering these commands, if you'd like to verify that they've gone
through correctly you can use `firewall-cmd` to list all enabled exceptions to
the firewall:

```bash
sudo firewall-cmd --permanent --list-all
```
```
public
	target: default
	icmp-block-inversion: no
	interfaces:
	sources:
	services: dhcpv6-client http https ssh
	ports:
	protocols:
	masquerade: no
	forward-ports:
	source-ports:
	icmp-blocks:
	rich rules:
```

After this, you should be able to try and reach the site again with better (but
definitely not finished!) results.  You should see a simple "404 Not Found"
page, indicating that Nginx received the request but could not find a file to
send you.  That's okay, we'll add the files in just a bit.  First, let's enable
https.

## Enable HTTPS using Let's Encrypt
Let's Encrypt is a wonderful and free service that provides SSL certificates
that use the most modern and up-to-date security standards.  What's even better
is that securing such a certificate could not be more simple.  Let's get going!

First, install their tool `certbot`:

```bash
sudo yum install certbot-nginx
```

Then, simply tell certbot to obtain a certificate for your domain:

```bash
sudo certbot --nginx -d learn.caes.uga.edu
```

Note that if you have additional domains, you would enter them here with
additional `-d` flags.  The following is an example:

```bash
sudo certbot --nginx -d learn.caes.uga.edu -d www.learn.caes.uga.edu
```

Certbot will run and will prompt you with a few things, namely the email
address to be contacted by Let's Encrypt with notices about expiring
certificates and the like, whether or not you'd like to be added to the EFF's
mailing list, and then whether or not you'd like all traffic to be routed to
HTTPS.  It's strongly recommended to do so, as HTTP is considered insecure by
all major browsers these days.

### Set Up Auto-Renewal
Let's Encrypt's certificates are only valid for ninety days.  This is to
encourage users to automate their certificate renewal process.  We'll need to
set up a regularly fun command to check for expiring certificates and renew them
automatically.

To run the renewal check daily, we will use `cron`, a standard system service
for running periodic jobs.  We tell `cron` what to do by opening and editing a
file called a `crontab`.

```bash
sudo crontab -e
```

Your text editor will open the default crontab which is an empty text file at
this point.  Paste in the following line, then save and close it:

```
15 3 * * * /usr/bin/certbot renew --quiet
```

The `15 3 * * *` part of this line means "run the following command at 3:15 am,
every day".  You may choose any time.

The `renew` command for Certbot will check all certificates installed on the
system and update any that are set to expire in less than thirty days.
`--quiet` tells Certbot not to output information or wait for user input.

`cron` will now run this command daily.  All installed certificates will be
automatically renewed and reloaded when they have thirty days or less before
they expire.

## Deploy Moodle Code
### Create Folders
Before we can deploy Moodle, we need to create a couple of folders.  When you
configured your Nginx server block, you already named one of these folders:
`/var/www/{YOUR_DOMAIN}/html`.  The other folder will live right alongside the
first: `/var/www/{YOUR_DOMAIN}/data`.  You can create these two directories
with the following command (using the "learn.caes.uga.edu" domain as an
example):

```bash
sudo mkdir -p /var/www/learn.caes.uga.edu/{html,data}
```

Now, we also need to make sure that Nginx has the ability to read and write to
these folders.  We do this by making the `nginx` user and the `nginx` group the
owners of these folders:

```bash
sudo chown -R nginx:nginx /var/www/learn.caes.uga.edu
```
```bash
sudo chmod -R 775 /var/www/learn.caes.uga.edu
```

We also need to set a special permission on the data folder, as required by
Moodle.  This permission will make it so that any file created within this
directory will inherit the group of the directory.  For the `data` directory
this is important since it is where all uploaded files will be placed, and those
files need to be owned by the `nginx` group.

```bash
sudo chmod 02777 /var/www/learn.caes.uga.edu/data
```

With that, we're basically set.  There'll be a few more permissions changes
we'll have to do later on, but we'll cross that bridge when we get to it.

### Download Moodle
In a terminal, use the following two commands to 1) move to your home directory
and 2) download the latest copy of Moodle.

```bash
cd ~
```
```bash
curl 'https://download.moodle.org/stable37/moodle-3.7.3.tgz' -o moodle.tgz
```

Once the download is complete, extract moodle and then copy the contents to
the web root we created earlier:

```bash
tar -xvf moodle.tgz
```
```bash
sudo mv moodle/* moodle/.* /var/www/learn.caes.uga.edu/html/
```

You may see a couple of warning messages here, but it's okay to ignore them.

Now, we need to make sure that Nginx has read and write permissions to all the
files we copied:

```bash
sudo chown -R nginx:nginx /var/www/learn.caes.uga.edu/html
```
```bash
sudo chmod -R u+rw,g+rw,o+r /var/www/learn.caes.uga.edu/html
```

With that, we can move on.

### Create a Database
Before Moodle can be configured, we need to create a database for it to work
with.  First, log in to MariaDB as the root user again:

```bash
mysql -u root -p
```

Then, enter the following two commands to 1) create a database for moodle and
2) create a user for moodle to act as when interacting with the database.  Be
sure to change 'yourpassword' to the password you'd like the moodle user to
use:

```sql
CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'moodleuser'@'localhost' IDENTIFIED BY 'yourpassword';
exit;
```

### Configure Moodle
Now we need to create and edit Moodle's config file.  So, copy the example that
ships with Moodle and then open the copy in a text editor:

```bash
cd /var/www/learn.caes.uga.edu/html
```
```bash
sudo cp config-dist.php config.php
```
```bash
sudo chown nginx:nginx config.php
```
```bash
sudo chmod 660 config.php
```
```bash
sudo -e config.php
```

We need to change a few things in a few sections here:

#### Database Setup

-	`$CFG->dbtype` needs to be changed from 'pgsql' to 'mariadb'
-	`$CFG->dbuser` needs to be changed from 'username' to 'moodleuser'
-	`$CFG->dbpass` needs to be changed from 'password' to the password you
	set above.

#### Website Location

-	`$CFG->wwwroot` needs to be changed to the full url for the domain  you're
	using.  E.g. "https://learn.caes.uga.edu"

#### Datafiles Location

-	`$CFG->dataroot` needs to be changed to the data directory we created.  E.g.
	`/var/www/learn.caes.uga.edu/data`

#### Other Miscellaneous Settings
There are a few lines here that you should search for and uncomment:

```php
$CFG->xsendfile = 'X-Accel-Redirect';
```

```php
$CFG->xsendfilealiases = array(
	'/dataroot'/ => $CFG->dataroot
);
```

After all of this, save and close the file.

## MariaDB Tweaks Required by Moodle
Moodle requires a few changes to the default setup of MariaDB on CentOS.  These
changes are all default in newer versions of MariaDB, but since CentOS focuses
more on stability than having the latest versions of software we have to make
these changes ourselves.  To make these changes we'll need to log into MariaDB
as the root user.

```bash
mysql -u root -p
```
Once logged in, the first change is to switch from the "Antelope" file format
to the newer "Barracuda" file format.  The following commands will make this
change:

```sql
SET GLOBAL innodb_file_format = "Barracuda";
SET GLOBAL innodb_file_format_max = "Barracuda";
SET GLOBAL innodb_file_per_table = "ON";
SET GLOBAL innodb_strict_mode = "ON";
```

It is also necessary to allow large prefixes in MariaDB:

```sql
SET GLOBAL innodb_large_prefix = "ON";
```

Once that's done, you can safely exit the MariaDB session:

```sql
exit;
```

## Whitelist Moodle Folders in SELinux
CentOS uses a security module called "Security-Enhanced Linux", or "SELinux"
for short.  To briefly summarize, this is a module for the Linux kernel
developed by the NSA and Red Hat that provides a mechanism for supporting
access control security policies.  Even more briefly: it further secures a
Linux system.

One of these security policies, however, prevents Nginx from reading and
writing to the above-mentioned directories.  As such, we need to run a few more
commands before we're okay to proceed.  The first will give Nginx read
permissions on the `html` directory; the second will give Nginx read
_and write_ permissions on the `data` directory:

```bash
sudo chcon -Rt httpd_sys_content_t /var/www/learn.caes.uga.edu/html
```
```bash
sudo chcon -Rt httpd_sys_rw_content_t /var/www/learn.caes.uga.edu/data
```

We also need to allow write access to Moodle's plugins folder, to allow
additional plugins to be installed:

```bash
sudo chcon -Rt httpd_sys_rw_content_t /var/www/learn.caes.uga.edu/html/plugins
```

With that, Nginx now has all the permissions it needs to be able to serve up
a Moodle website!

## Run Moodle Installer
At this point, we should have a perfectly-configured server ready for Moodle to
be installed on.  In your browser, simply visit the url of your server (e.g.
https://learn.caes.uga.edu).

You should (hopefully) be greeted by the Moodle installation dialogue.  The
first page simply asks if you have read and understand Moodle's terms and
conditions.  When you have, click "Continue" to move on.

The next page will conduct a big list of server checks.  Since we've already
done everything needed here, everything listed should have a green "OK" status
marker at the end of the row, and there should be a green notice at the bottom
of the table that reads "Your server environment meets all minimum
requirements.".  Click "Continue" when you're ready to move on.

The next page might take a while to load, and that's because Moodle is working
hard in the background getting the database set up and configured.  Just sit
tight and when it's done you should see a big list of items with a bunch of
"Success" notices.  Yet another "Continue" button will be at the bottom of the
page; click it to move on.

The next page is the profile configuration page for the default admin account.
Configure it as you desire, then click "Update profile" to save these changes
and continue.

Finally, you will be dropped at the page to configure your Moodle site.  At this
point, it's really up to you how you configure the site.  So, do what you wish
and enjoy your brand new Moodle installation on CentOS 7!
